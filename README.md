# Codewars

Solutions of interesting and non-trivial katas from Codewars.com.

Each task has a directory with a package which typically consists of up to three files:

- <PackageName>.hs is the full source code for the corresponding task, which may be directly copy&pasted into Codewars.
- Test.hs (optional) is the test suite taken from Codewars.
- Main.hs (optional) is a console wrapper for manual testing.