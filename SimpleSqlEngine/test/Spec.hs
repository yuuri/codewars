import SimpleSQLEngine.Kata
import Test.Hspec
import Data.Either (isLeft)
import Data.List (sort)

movieDatabase = [ ( "movie"
                  , [ [ ( "id", "1" ), ( "name", "Avatar"   ), ( "directorID", "1" ) ]
                    , [ ( "id", "2" ), ( "name", "Titanic"  ), ( "directorID", "1" ) ]
                    , [ ( "id", "3" ), ( "name", "Infamous" ), ( "directorID", "2" ) ]
                    , [ ( "id", "4" ), ( "name", "Skyfall"  ), ( "directorID", "3" ) ]
                    , [ ( "id", "5" ), ( "name", "Aliens"   ), ( "directorID", "1" ) ]
                    ]
                  )
                , ( "actor"
                  , [ [ ( "id", "1" ), ( "name", "Leonardo DiCaprio" ) ]
                    , [ ( "id", "2" ), ( "name", "Sigourney Weaver"  ) ]
                    , [ ( "id", "3" ), ( "name", "Daniel Craig"      ) ]
                    ]
                  )
                , ( "director"
                  , [ [ ( "id", "1" ), ( "name", "James Cameron"   ) ]
                    , [ ( "id", "2" ), ( "name", "Douglas McGrath" ) ]
                    , [ ( "id", "3" ), ( "name", "Sam Mendes"      ) ]
                    ]
                  )
                , ( "actor_to_movie"
                  , [ [ ( "movieID", "1" ), ( "actorID", "2" ) ]
                    , [ ( "movieID", "2" ), ( "actorID", "1" ) ]
                    , [ ( "movieID", "3" ), ( "actorID", "2" ) ]
                    , [ ( "movieID", "3" ), ( "actorID", "3" ) ]
                    , [ ( "movieID", "4" ), ( "actorID", "3" ) ]
                    , [ ( "movieID", "5" ), ( "actorID", "2" ) ]
                    ]
                  )
                ]

shouldParse str res = parseQuery str `shouldBe` Right res

shouldFail = isLeft . parseQuery

main =
  hspec $ do
    describe "parsing" $ do
      it "should parse trivial queries" $ do
        "select movie.name from movie" `shouldParse` Query [("movie", "name")] "movie" [] Nothing
        "sElEcT movie.name FRoM movie" `shouldParse` Query [("movie", "name")] "movie" [] Nothing
      it "should handle multiple columns" $
        "Select movie.name, director.name From movie" `shouldParse`
          Query [("movie", "name"), ("director", "name")] "movie" [] Nothing
      it "should handle joins" $
        "SelecT movie.name,director.name\nFroM director\nJoiN movie ON director.id = movie.directorID\n" `shouldParse`
          Query
            [("movie", "name"), ("director", "name")]
            "director"
            [Join "movie" $ Condition (ArgColumn ("director", "id")) OpEq (ArgColumn ("movie", "directorID"))]
            Nothing
      it "should handle the `where` clause" $
        "SELECT movie.name FROM movie WHERE movie.directorID = '1'" `shouldParse`
          Query [("movie", "name")] "movie" []
            (Just $ Condition (ArgColumn ("movie", "directorID")) OpEq (ArgVal "1"))
      it "should handle escaped strings" $
        "SELECT movie.name FROM movie WHERE movie.title = 'Hit''n''Run'" `shouldParse`
          Query [("movie", "name")] "movie" []
            (Just $ Condition (ArgColumn ("movie", "title")) OpEq (ArgVal "Hit'n'Run"))
    
    describe "execution" $ do
      let execute = sqlEngine movieDatabase
      it "should SELECT columns" $
        sort (execute "select movie.name from movie")
          `shouldBe` [ [ ( "movie.name", "Aliens"   ) ]
                     , [ ( "movie.name", "Avatar"   ) ]
                     , [ ( "movie.name", "Infamous" ) ]
                     , [ ( "movie.name", "Skyfall"  ) ]
                     , [ ( "movie.name", "Titanic"  ) ]
                     ]
    {-
      it "should apply WHERE" $
        sort (execute "SELECT movie.name FROM movie WHERE movie.directorID = '1'")
          `shouldBe` [ [ ( "movie.name", "Aliens"  ) ]
                     , [ ( "movie.name", "Avatar"  ) ]
                     , [ ( "movie.name", "Titanic" ) ]
                     ]
      it "should perform parent->child JOIN" $
        sort (execute "Select movie.name, director.name From movie Join director On director.id = movie.directorID")
          `shouldBe` [ [ ( "movie.name", "Aliens"   ), ( "director.name", "James Cameron"   ) ]
                     , [ ( "movie.name", "Avatar"   ), ( "director.name", "James Cameron"   ) ]
                     , [ ( "movie.name", "Infamous" ), ( "director.name", "Douglas McGrath" ) ]
                     , [ ( "movie.name", "Skyfall"  ), ( "director.name", "Sam Mendes"      ) ]
                     , [ ( "movie.name", "Titanic"  ), ( "director.name", "James Cameron"   ) ]
                     ]
      it "should perform child->parent JOIN" $
        sort (execute "SelecT movie.name,director.name\nFroM director\nJoiN movie ON director.id = movie.directorID\n")
          `shouldBe` [ [ ( "movie.name", "Aliens"   ), ( "director.name", "James Cameron"   ) ]
                     , [ ( "movie.name", "Avatar"   ), ( "director.name", "James Cameron"   ) ]
                     , [ ( "movie.name", "Infamous" ), ( "director.name", "Douglas McGrath" ) ]
                     , [ ( "movie.name", "Skyfall"  ), ( "director.name", "Sam Mendes"      ) ]
                     , [ ( "movie.name", "Titanic"  ), ( "director.name", "James Cameron"   ) ]
                     ]
      it "should perform many-to-many JOIN and apply WHERE" $
        sort (execute "select movie.name   \
                      \     , actor.name\
                      \  FROM movie\
                      \  Join actor_to_movie\
                      \       oN actor_to_movie.movieID=movie.id\
                      \  JoIn actor\
                      \    oN actor_to_movie.actorID = actor.id\
                      \ WheRe \n\
                      \   actor.name <> 'Daniel Craig'")
          `shouldBe` [ [ ( "movie.name", "Aliens"   ), ( "actor.name", "Sigourney Weaver"  ) ]
                     , [ ( "movie.name", "Avatar"   ), ( "actor.name", "Sigourney Weaver"  ) ]
                     , [ ( "movie.name", "Infamous" ), ( "actor.name", "Sigourney Weaver"  ) ]
                     , [ ( "movie.name", "Titanic"  ), ( "actor.name", "Leonardo DiCaprio" ) ]
                     ]
    -}