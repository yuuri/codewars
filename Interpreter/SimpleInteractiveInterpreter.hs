module SimpleInteractiveInterpreter
    ( Interpreter
    , newInterpreter
    , Result
    , input
    ) where

import Control.Monad.State
import Control.Monad.Except
import Data.Char
import qualified Data.Map as M
import Data.Maybe (isJust)
import Data.Monoid
import qualified Data.Set as S
import Text.ParserCombinators.ReadP

-- * Language definitions

type Value = Double

type VarName = String
type FunName = String

data BinOp = Add | Sub | Mul | Div | Mod
    deriving Show

data Expression
    = Const Value | Var VarName
    | Assign VarName Expression
    | Funcall FunName [Expression]
    | Binary Expression BinOp Expression
    deriving Show

usedVars :: Expression -> S.Set VarName
usedVars expr = case expr of
    Var name     -> S.singleton name
    Binary l _ r -> usedVars l `S.union` usedVars r
    Assign _ e   -> usedVars e
    Funcall _ es -> mconcat $ map usedVars es
    _            -> S.empty

data FunDef = FunDef [VarName] Expression
    deriving Show

type Statement = Either (FunName, FunDef) Expression

-- * Parsing

type FunArity = M.Map FunName Int
getArity :: FunArity -> FunName -> Maybe Int
getArity = flip M.lookup

runParser :: FunArity -> String -> Either String Statement
runParser funDict str = case filter (null . snd) parsed of
    ((res, ""):_) -> Right res
    smth -> Left $ "Parsing error, partial results: " ++ show parsed
    where
        skipSpaces0 = munch isSpace *> return ()

        isIdentChar '_' = True
        isIdentChar c = isDigit c || isAlpha c

        pIdent = (:) <$> satisfy isAlpha <*> munch isIdentChar

        pVarName = pIdent
        pFunName = pIdent

        pFunction = do
            name <- string "fn" *> skipSpaces *> pFunName
            args <- manyTill (skipSpaces *> pVarName) $ between skipSpaces0 skipSpaces0 (string "=>")
            body <- pExpr
            case filter (`S.notMember` usedVars body) args of
                []   -> return (name, FunDef args body)
                vars -> pfail --throwError $ "Variables " ++ unwords vars ++ " are not in the function arguments"

        pAssign = Assign <$> pVarName <*> (between skipSpaces0 skipSpaces0 (char '=') *> pExpr)

        pVarOrFuncall = do
            name <- pFunName
            case getArity funDict name of
                Just arity -> do
                    args <- replicateM arity $ skipSpaces *> pExpr
                    return $ Funcall name args
                --If there is no such function, condider it a variable
                Nothing -> return $ Var name
                                            
        pNumber = do
            int <- munch1 isDigit
            frac <- option "" $ (:) <$> char '.' <*> munch1 isDigit
            return $ read $ int ++ frac

        pExpr = pAssign <++ do
            init <- pFactor
            rest <- many $ (,) <$> between skipSpaces0 skipSpaces0 pAddOp <*> pFactor
            return $ foldl (\l (op, r) -> Binary l op r) init rest

        pFactor = do
            init <- pVal
            rest <- many $ (,) <$> between skipSpaces0 skipSpaces0 pMulOp <*> pVal
            return $ foldl (\l (op, r) -> Binary l op r) init rest

        pVal  = between (char '(' <* skipSpaces0) (skipSpaces0 *> char ')') pExpr
            <++ (Const <$> pNumber)
            <++ pVarOrFuncall
              -- <++ (Var <$> pVarName)

        pAddOp = pOps [('+', Add), ('-', Sub)]
        pMulOp = pOps [('*', Mul), ('/', Div), ('%', Mod)]

        pStatement = (Left <$> pFunction) <++ (Right <$> pExpr)

        parsed = readP_to_S pStatement str

        pOps = choice . map (\(c, cons) -> char c *> pure cons)

-- * Evaluating of expressions

-- ** Memory (a storage for variables)

data Memory = Memory { memLocals, memGlobals :: M.Map VarName Value }

emptyMemory :: Memory
emptyMemory = Memory M.empty M.empty

memGet :: VarName -> Memory -> Maybe Value
memGet name (Memory locals globals) = M.lookup name locals `mplus` M.lookup name globals
    --Smartass variant: getFirst $ mconcat $ map (First . (M.!? name)) [locals, globals]
    --Verbose variant: case locals M.!? name of
    --    Nothing -> globals M.!? name
    --    res -> res

memPut :: VarName -> Value -> Memory -> Memory
memPut name value memory@Memory {memLocals = locals, memGlobals = globals}
    | name `M.member` locals = memory {memLocals  = M.insert name value locals }
    | otherwise              = memory {memGlobals = M.insert name value globals}

-- ** Evaluator

type Evaluator = StateT Memory (Except String) Value

runEvaluator :: Memory -> Evaluator -> Either String (Value, Memory)
runEvaluator mem ev = runExcept $ runStateT ev mem

assign :: VarName -> Double -> Evaluator
assign name value = modify (memPut name value) >> return value

peek :: VarName -> Evaluator
peek name = gets (memGet name) >>= maybe (throwError $ "Undeclared variable: " ++ name) return

-- * Interpreting of statements

type FunDict = M.Map FunName FunDef

getArities :: FunDict -> FunArity
getArities = M.map (\(FunDef args _) -> length args)

data Interpreter = Interpreter { ipMemory :: Memory, ipFunctions :: FunDict }
newInterpreter = Interpreter emptyMemory M.empty

defun :: FunName -> FunDef -> Interpreter -> Interpreter
defun name def interpreter = interpreter { ipFunctions = M.insert name def $ ipFunctions interpreter }

opFunc :: BinOp -> (Value -> Value -> Value)
opFunc op = case op of
    Add -> (+)
    Sub -> (-)
    Mul -> (*)
    Div -> rounded quot
    Mod -> rounded rem
    where
        rounded f a b = fromIntegral $ f (round' a) (round' b)
        round' = round :: Double -> Int

eval :: Interpreter -> Expression -> Either String (Value, Interpreter)
eval (Interpreter mem funDict) expr =
    fmap (\newMem -> Interpreter newMem funDict) <$> runEvaluator mem (eval' expr) where
        eval' :: Expression -> Evaluator
        eval' (Const v) = return v
        eval' (Var name) = peek name
        eval' (Assign name expr) = do
            when (name `M.member` funDict) $
                throwError $ "A function with name " ++ name ++ " is already declared"
            eval' expr >>= assign name
        eval' (Binary left op right) = opFunc op <$> eval' left <*> eval' right
        eval' (Funcall name args) = case M.lookup name funDict of
            Nothing -> throwError $ "Undeclared function: " ++ name
            Just (FunDef funArgs body) -> do
                let expectedNumArgs = length funArgs
                    numArgs = length args
                when (expectedNumArgs /= numArgs) $ throwError $ concat
                    [ "Function ", name, " expects "
                    , show expectedNumArgs, " arguments, but got ", show numArgs
                    ]
                argVals <- mapM eval' args
                let locals = M.fromList $ zip funArgs argVals
                localMem <- gets $ \mem -> mem { memLocals = locals }
                case eval (Interpreter localMem funDict) body of
                    Left err -> throwError err
                    Right (res, interpreter') -> put (ipMemory interpreter') >> return res

-- * Running the interpreter

type Result = Maybe Value

input :: String -> Interpreter -> Either String (Result, Interpreter)
input str interpreter
    | all isSpace str = Right (Nothing, interpreter)
    | otherwise = do
        stmt <- runParser (getArities $ ipFunctions interpreter) str
        case stmt of
            Left (name, def) -> do
                when (isJust $ memGet name $ ipMemory interpreter) $
                    throwError $ "A variable with name " ++ name ++ " is already declared"
                return (Nothing, defun name def interpreter)
            Right expr -> case eval interpreter expr of
                Right (res, newInterpreter) -> Right (Just res, newInterpreter)
                Left err -> Left err
