import SimpleInteractiveInterpreter

import Control.Monad
import System.IO

{-
runScript :: Interpreter -> [String] -> ([Either String Result], Interpreter)
runScript interpreter [] = ([], interpreter)
runScript interpreter (x:xs) = case input stmt interpreter of
    Left err ->
-}

main = do
    hSetBuffering stdin LineBuffering
    let step interpreter stmt = case input stmt interpreter of
            Left err -> putStrLn err >> return interpreter
            Right (res, interpreter') -> maybe (return ()) print res >> return interpreter'
    stmts <- lines <$> getContents
    foldM step newInterpreter stmts
    {-let (results, _) = runCalculator newInterpreter stmts
    forM_ results $ \res -> case res of
        Left err -> putStrLn err
        Right res -> maybe (return ()) (putStrLn . show) res-}
