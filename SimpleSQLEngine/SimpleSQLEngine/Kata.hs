module SimpleSQLEngine.Kata where

import Data.Bifunctor (first, second)
import Data.Maybe (fromJust)
import Text.Parsec
import Text.Parsec.Language (emptyDef)
import qualified Text.Parsec.Token as P

{- * Database -}

type Database = [(TableName, Table)]

type TableName = String

getTable :: TableName -> Database -> Table
getTable table = fromJust . lookup table

type Table = [Row]

type Row = [Cell]

type Cell = (ColumnId, Value)

type ColumnId = (TableName, ColumnName)

showColumnId (tab, col) = tab ++ '.':col

--type ColumnId = String

type ColumnName = String

rowSelect :: [ColumnId] -> Row -> Row
rowSelect columns = filter ((`elem` columns) . fst)

tableSelect :: [ColumnId] -> Table -> Table
tableSelect columns = map (rowSelect columns)

--data Value = CDouble Double | CStr String
--    deriving Eq

type Value = String

{- * Queries -}

data Query = Query
    { querySelect :: [ColumnId]
    , queryFrom   :: TableName
    , queryJoins  :: [Join]
    , queryWhere  :: Maybe Condition
    } deriving (Eq, Show)

data Join = Join TableName Condition
    deriving (Eq, Show)

data Condition = Condition Arg CondOp Arg
    deriving (Eq, Show)

data CondOp = OpEq | OpGT | OpLT | OpLE | OpGE | OpNE
    deriving (Eq, Show)

data Arg = ArgColumn ColumnId | ArgVal Value
    deriving (Eq, Show)

{- * Parsing -}

P.TokenParser
    { P.commaSep1 = commaSep1
    , P.dot = dot
    , P.float = float
    , P.identifier = identifier
    , P.lexeme = lexeme
    , P.operator = operator
    , P.reserved = reserved
    } = P.makeTokenParser emptyDef
    { P.caseSensitive = False
    , P.identStart = letter
    , P.identLetter = alphaNum
    , P.opLetter = oneOf "=<>"
    , P.reservedNames = ["select", "from", "join", "on", "where"]
    , P.reservedOpNames = ["<", "<=", ">", ">=", "=", "<>"]
    }

type Parser a = Parsec String () a

{- Grammar
query         =  select, ws, from, [ ws, join ], [ ws, where ] ;
select        =  "SELECT ", column-id, [ { ", ", column-id } ] ;
from          =  "FROM ", table-name, [ { ws, join } ] ;
join          =  "JOIN ", table-name, " on ", value-test ;
where         =  "WHERE ", value-test ;
value-test    =  value, comparison, value;
column-id     =  table-name, ".", column-name ;
table-name    = ? a valid SQL table name ? ;
column-name   = ? a valid SQL column name ? ;
value         =  column-id | const
comparison    =  " = " | " > " | " < " | " <= " | " >= " | " <> " ;
const         =  ? a number ? | ? a SQL single-quoted string ? ;
ws            = " " | "\n" | ws, ws ;
-}

pQuery :: Parser Query
{-
pQuery = do
    reserved "select"
    columns <- commaSep1 pColumnId
    reverved "from"
    table <- pTableName
    joins <- optional (return []) $ many pJoin
    clause <- optionMaybe $ reserved "where" *> pClause
    return $ Query columns table joins clause
-}
pQuery = Query
    <$> (reserved "select" *> commaSep1 pColumnId)
    <*> (reserved "from" *> pTableName)
    <*> (option [] $ many pJoin)
    <*> (optionMaybe $ reserved "where" *> pCondition)

pColumnId :: Parser ColumnId
pColumnId = (,) <$> pTableName <*> (dot *> identifier)

pTableName :: Parser TableName
pTableName = identifier

pJoin :: Parser Join
pJoin = Join <$> (reserved "join" *> pTableName) <*> (reserved "on" *> pCondition)

pCondition :: Parser Condition
pCondition = Condition <$> pArg <*> pOperator <*> pArg

pOperator :: Parser CondOp
pOperator = choice $ map (\(str, op) -> reserved str >> pure op)
    [ (">", OpGT), (">=", OpGE)
    , ("<", OpLT), ("<=", OpLE)
    , ("=", OpEq), ("<>", OpNE)
    ]

pArg :: Parser Arg
pArg = ArgColumn <$> pColumnId <|> ArgVal <$> pValue

pValue :: Parser Value
pValue = pString --CStr <$> pString <|> CDouble <$> float

pString :: Parser String
pString = lexeme $ between tick tick $ many stringLetter where
    tick = char '\''
    stringLetter = try (tick *> tick) <|> satisfy (/= '\'')

parseQuery :: String -> Either String Query
parseQuery = first show . parse pQuery ""

{- * Execution -}

type InputTable = [[(String, String)]]

type InputDatabase = [(String, InputTable)]

fromInput :: InputDatabase -> Database
fromInput = map toTable where
    toTable :: (String, [[(String, String)]]) -> (TableName, Table)
    toTable (tableName, rows) = (tableName, map toRow rows) where
        toRow :: [(String, String)] -> Row
        toRow = map toCell
        toCell :: (String, String) -> Cell
        toCell (colName, val) = ((tableName, colName), val)

type Result = InputTable

toResult :: Table -> Result
toResult = map fromRow where
    fromRow :: Row -> [(String, String)]
    fromRow = map (first showColumnId)

execute :: Query -> Database -> Table
execute Query
    { querySelect = columnIds
    , queryFrom   = table
    }
    = tableSelect columnIds . getTable table

sqlEngine :: InputDatabase -> String -> Result
sqlEngine database queryStr = case parseQuery queryStr of
    Left _      -> error "This shouldn't have happened"
    Right query -> toResult . execute query $ fromInput database
