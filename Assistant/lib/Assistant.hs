{-# LANGUAGE OverloadedStrings #-}

module Assistant
    ( module Model
    , getKata
    , suggestName
    , createPackage
    ) where

import Control.Monad (forM_)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import System.Directory
import System.IO
import Network.HTTP.Simple

import Model

type KataId = String

suggestName :: Kata -> String
suggestName = T.unpack . mconcat . map T.toTitle . T.splitOn "-" . kataSlug

writeLog :: String -> IO ()
writeLog = hPutStrLn stderr

getKata :: KataId -> IO Kata
getKata kataId = do
    writeLog $ "Getting info for the kata " ++ kataId ++ "..."
    req <- parseRequest $ "https://www.codewars.com/api/v1/code-challenges/" ++ kataId
    response <- httpJSON req
    writeLog $ "The status code was: " ++ show (getResponseStatusCode response)
    writeLog $ show $ getResponseHeader "Content-Type" response
    return (getResponseBody response :: Kata)

mkDirLog :: FilePath -> IO ()
mkDirLog path = writeLog ("Creating " ++ path) >> createDirectory path

copyFileLog :: FilePath -> FilePath -> IO ()
copyFileLog src dst = writeLog ("Copying from " ++ src ++ " to " ++ dst) >> copyFile src dst

createPackage :: FilePath -> String -> Kata -> IO ()
createPackage templates name kata = do
    let dirName = name ++ "/"
    mkDirLog dirName
    --let mainFile = "Main.hs"
    writeFile (dirName ++ name ++ ".hs") $ "module " ++ name ++ " where\n"
    T.writeFile (dirName ++ "README.md") $ kataDescription kata
    let testsDir = dirName ++ "test/"
    mkDirLog testsDir
    forM_ ["package.yaml", "test/Spec.hs"] $ \file ->
        copyFileLog (templates ++ file) (dirName ++ file)

assist :: IO ()
assist = undefined