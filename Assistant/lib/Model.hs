{-# LANGUAGE OverloadedStrings #-}

module Model where

import Data.Aeson
import qualified Data.Text as T

data Kata = Kata
    { kataId :: T.Text
    , kataName :: T.Text
    , kataSlug :: T.Text
    , kataDescription :: T.Text
    }

unpack :: T.Text -> String
unpack = T.unpack

instance Show Kata where
    show (Kata id name slug descr) = unlines
        [ "==== " ++ T.unpack name ++ " ===="
        , "Id: " ++ T.unpack id
        , T.unpack (T.take 100 descr) ++ " <...>"
        ]

instance FromJSON Kata where
    parseJSON = withObject "Person" $ \v -> Kata
        <$> v .: "id"
        <*> v .: "name"
        <*> v .: "slug"
        <*> v .: "description"