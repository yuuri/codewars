import System.IO
import System.Environment
import System.Exit

import Assistant

printHelp :: IO ()
printHelp = putStrLn $ unlines $
    [ "info <kata> - show information about a kata (-d to show the full description)"
    , "help - show this help"
    ]

newPackage :: Kata -> IO ()
newPackage kata = do
    let suggestedName = suggestName kata
    putStr $ "Enter the package name (" ++ suggestedName ++ "): "
    inputName <- getLine
    let packageName = if null inputName then suggestedName else inputName
    createPackage "templates/" packageName kata

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering
    (cmd:args) <- getArgs
    case (cmd, args) of
        ("help", []) -> printHelp
        ("info", [kataId]) -> getKata kataId >>= print
        ("info", [kataId, "-d"]) -> getKata kataId >>= putStrLn . unpack . kataDescription
        ("new", [kataId]) -> getKata kataId >>= newPackage
        _ -> putStrLn "Unknown command" >> exitFailure